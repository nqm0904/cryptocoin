import 'package:cryptocoin/model/model.dart';
import 'package:flutter/material.dart';

class CoinChartWidget extends StatelessWidget {
  final List<ChartData> data;
  final UsdModel coinPrice;
  final Color color;

  const CoinChartWidget({
    Key? key,
    required this.data,
    required this.coinPrice,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          color == Colors.green ? Container() : Container(
            padding: const EdgeInsets.all(4.0),
            margin: const EdgeInsets.only(left: 64.0),
            alignment: Alignment.center,
            width: 72,
            height: 36,
            decoration: BoxDecoration(
              color: coinPrice.percentChange_7d >=0
                  ? Colors.green:Colors.red,
              borderRadius: BorderRadius.circular(16.0)
            ),
            child: Text(
              coinPrice.percentChange_7d.toStringAsFixed(2)+"%",
              style: TextStyle(
                color: Colors.white,fontWeight: FontWeight.bold
              ),
            ),
          ),
        ],
      ),
    );
  }
}
