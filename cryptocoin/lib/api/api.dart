import 'package:dio/dio.dart';
import 'package:cryptocoin/model/big_data_model.dart';
class Api{
  static String Url = "https://pro-api.coinmarketcap.com/v1/";
  final String apiKey = "43dc93bd-a7fe-43e5-8680-5b4f5e6d90ee";
  var currencyListAPI = '${Url}cryptocurrency/listings/latest';
  Dio _dio = Dio();
  Future<BigDataModel> getCoins() async{
    try{
      _dio.options.headers["X-CMC_PRO_API_KEY"] = apiKey;
      Response response = await _dio.get(currencyListAPI);
      return BigDataModel.fromJson((response.data));
    }catch(error,stacktrace){
      print("Exception occured: $error stackTrace $stacktrace");
      return BigDataModel.withError("error");
    }
  }
}