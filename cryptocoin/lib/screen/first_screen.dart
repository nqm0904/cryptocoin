import 'package:cryptocoin/api/api.dart';
import 'package:cryptocoin/model/big_data_model.dart';
import 'package:cryptocoin/widget/widget.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  late Future<BigDataModel> _futureCoins;
  late Api api;

  @override
  void initState() {
    api = Api();
    _futureCoins = api.getCoins();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<BigDataModel>(
        future: _futureCoins,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              var coinsData = snapshot.data!.dataModel;
              return CoinListWidget(coins: coinsData);
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
